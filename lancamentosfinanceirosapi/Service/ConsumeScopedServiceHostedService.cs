using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using System;
using Microsoft.Extensions.Logging;
using LancamentosFinanceiro.Repository;

namespace LancamentosFinanceiro.Service
{
    internal class ConsumeScopedServiceHostedService : IHostedService
    {
        private readonly ILogger _logger;

        public ConsumeScopedServiceHostedService(ILogger<ConsumeScopedServiceHostedService> logger)
        {            
            _logger = logger;
        }       

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is starting.");

            DoWork();

            return Task.CompletedTask;
        }

        private void DoWork()
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is working.");

            Console.WriteLine("DoWork");
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation(
                "Consume Scoped Service Hosted Service is stopping.");

            return Task.CompletedTask;
        }
    }
}