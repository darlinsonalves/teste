﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LancamentosFinanceiro.Repository;
using LancamentosFinanceiro.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Cors;

namespace LancamentosFinanceiro.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [EnableCors("MyPolicy")]
    public class ContaController : ControllerBase
    {
        private readonly IContaRepository _repository;
        public ContaController(IContaRepository repository)
        {
            _repository = repository;
        }

        [HttpGet]        
        public async Task<ActionResult<IEnumerable<Conta>>> GetContas()
        {
            return await _repository.Find();
        }

        [HttpPost]
        [Route("import")]
        public IActionResult Import([FromBody]List<Conta> value)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            else
            {
                List<Conta> data = value;
                return Ok();
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<Conta>> GetConta(long id)
        {
            var conta = await _repository.GetConta(id);
            if(conta == null)
            {
                return NotFound();
            }
            return Ok(conta);
        }

        [HttpGet("login/{numero}")]
        public async Task<ActionResult<Conta>> Login(string numero)
        {
            var conta = await _repository.GetAlComplete(numero);            
            return Ok(conta);
        }

        [HttpPost]
        public async Task<ActionResult<Conta>> PostConta([FromBody]Conta conta)
        {
            try
            {                
                return Ok(await _repository.PostConta(conta));
            } catch(InvalidOperationException e )
            {
                return BadRequest(e.Message);
            } catch(Exception ie) {
                return BadRequest(ie.Message);
            }
        }
    }
}