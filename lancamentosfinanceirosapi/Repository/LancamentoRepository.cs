using LancamentosFinanceiro.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;


namespace LancamentosFinanceiro.Repository
{
    public class LancamentoRepository
    {
        private readonly LancamentoContext _context;
        
        public LancamentoRepository(LancamentoContext context)
        {
            _context = context;
            Console.WriteLine("LancamentoRepository");
            if(_context.Lancamentos.Count() == 0)
            {
                _context.Lancamentos.Add(new Lancamento {Data = "2018-11-03", Tipo = "entrada", Valor = "300", Descricao= "primeiro pagamento"});
                _context.SaveChanges();

            }
        }

        public async Task<ActionResult<IEnumerable<Lancamento>>> Find()
        {
            return await _context.Lancamentos.ToListAsync();
        }

        public async Task<Lancamento> PostLancamento(Lancamento lancamento)
        {
            _context.Lancamentos.Add(lancamento);
            await _context.SaveChangesAsync();

            return lancamento;
        }


       public virtual int Count()
       {
          return _context.Lancamentos.Count();
       }
        
    }
}