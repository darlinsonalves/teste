﻿using LancamentosFinanceiro.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LancamentosFinanceiro.Repository
{
    public class ContaRepository : IContaRepository
    {
        private readonly ContaContext _context;
        public ContaRepository(ContaContext context)
        {
            _context = context;
            if(_context.Contas.Count() == 0)
            {
                _context.Contas.Add(new Conta { Responsavel = "Darlinson Alves", Agencia = "3297-0", Numero = "010012361", Banco = "Santander", Id = 1, Tipo = "corrente" });
                _context.SaveChanges();

            }
        }

        public async Task<ActionResult<IEnumerable<Conta>>> Find()
        {
            return await _context.Contas.ToListAsync();
        }

        public async Task<Conta> GetConta(long id)
        {
            return await _context.Contas.FindAsync(id);
        }
        public async Task<Conta> PostConta(Conta conta)
        {
            _context.Contas.Add(conta);
            await _context.SaveChangesAsync();

            return conta;
        }

        public async Task<List<Conta>> GetAlComplete(String numero)
        {
            var query = _context.Contas.Where(v => v.Numero == numero);
            return await query.ToListAsync();
        }
    }
}
