﻿using LancamentosFinanceiro.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LancamentosFinanceiro.Repository
{
    public interface IContaRepository
    {
        Task<ActionResult<IEnumerable<Conta>>> Find();
        Task<Conta> GetConta(long id);
        Task<Conta> PostConta(Conta conta);
        Task<List<Conta>> GetAlComplete(String numero);      
    }
}
