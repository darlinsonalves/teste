using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LancamentosFinanceiro.Models 
{
    public class Lancamento
    {
        public long Id {get; set;}
        public string Data {get; set;}
        public string Tipo {get; set;}
        public string Valor {get; set;}
        public string Descricao {get; set;}
        public string Conta {get; set;}
        public string Agencia {get; set;}
    }
}