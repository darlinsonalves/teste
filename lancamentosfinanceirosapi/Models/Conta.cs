﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace LancamentosFinanceiro.Models
{
    public class Conta
    {
        public long Id { get; set; }
        [Required(ErrorMessage = "Campo Banco não pode ser vazio!")]
        public string Banco { get; set; }
        [Required(ErrorMessage = "Campo Agencia não pode ser vazio!")]
        public string Agencia { get; set; }
        [Required(ErrorMessage = "Numero da conta não pode ser vazio!")]
        [Key]
        public string Numero { get; set; }
        [Required(ErrorMessage = "Campo Tipo não pode ser vazio!")]
        public string Tipo { get; set; }
        [Required(ErrorMessage = "Campo Responsavel não pode ser vazio!")]
        public string Responsavel { get; set; }
    }
}
