/* Dependencies */
import React from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
/* Pusher */
import { setPusherClient } from 'react-pusher'
import pusherClient from './helpers/pusher';

/* Routes */
import Routes from './routes'

/* Reducers combined */
import store from './store'

/* Assets */
import './assets/css/style.min.css'

const options = {
    timeout: 5000,
    position: "bottom center"
  };

setPusherClient(pusherClient)

ReactDOM.render(    
    <Provider store={store}>
        <Routes />    
    </Provider>,
document.getElementById('root'))
