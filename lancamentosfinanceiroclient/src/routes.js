/**
 |--------------------------------------------------
 | ROTAS DA APLICAÇÃO
 | TODAS AS ROTAS DEVEM FICAR NESSE ARQUIVO!
 |--------------------------------------------------
 */

/* Dependencies */
import React, { Component, Fragment } from 'react'
import { Router, Route, Switch } from 'react-router-dom'
import { createBrowserHistory } from 'history'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import ContaFormContainer from './components/containers/ContaFormContainer';
import ListaContaContainer from './components/containers/ListaContasContainer';
import { NavbarLink} from './components/NavbarLink'
import { App } from './components/app';
import LancamentosListContainer from './components/containers/LancamentosListContainer';
import ImportCSVContainer from './components/containers/importCSVContainer';
import ProtectedLayoutContainer from './components/containers/ProtectedLayoutContainer';
import LoginFormContainer from './components/containers/LoginFormContainer';
import Notificacoes from './components/notificacao';



class Routes extends Component {
	render() {
		const history = createBrowserHistory()
		return (
			
			<Router history={history}>
				<Fragment>
					<Switch>
						<App>
							<Route exact path="/" component={LoginFormContainer} />
							<Route exact path="/cadastrar" component={ContaFormContainer} />
							<Route exact path="/list" component={ListaContaContainer} />
							<Route exact path="/import" component={ImportCSVContainer} />
							<Route exact path="/lancamentos" component={LancamentosListContainer} />							
						</App>
						<Notificacoes/>
					</Switch>
				</Fragment>
			</Router>
			
		)
	}
}

const mapDispatchToProps = dispatch => bindActionCreators({}, dispatch)

export default connect(null, mapDispatchToProps)(Routes)
