/* Dependencies */
import { combineReducers } from 'redux'
import { applyMiddleware, createStore } from 'redux'
import promisse from 'redux-promise'
import thunk from 'redux-thunk'

/* Reducers */
import { reducer as form } from 'redux-form'
import { reducer as toastr } from 'react-redux-toastr'
import contas from './reducers/contas'
import lancamentos from './reducers/lancamentos'

const reducers = combineReducers({
	form,
	contas,
	toastr,
	lancamentos
})

const devTools = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
const store = applyMiddleware(
	promisse,
	thunk	
)(createStore)(reducers, devTools)

export default store
