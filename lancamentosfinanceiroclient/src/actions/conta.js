import axios from 'axios'
import { toastr } from 'react-redux-toastr';

export const GET_CONTAS = 'GET_CONTAS'
export const GET_LANCAMENTOS = 'GET_LANCAMENTOS'

export const postConta = (data) => (
    (dispatch, getState) => {
           axios.post('http://localhost:5000/api/conta', data).then(response => {
            alert('Nova conta cadastrada!')
           }).catch(error => {
                Object.keys(error.response.data).map(msg => {
                    alert(error.response.data[msg][0])
                })
           })
    }
)

export const getContas = () => (
    async dispatch => {
        const response = await fetch('http://localhost:5000/api/conta')
        const data = await response.json()
        dispatch({type: GET_CONTAS, payload: data});
    }
)

export const getLancamentos = () => (
    dispatch => {
        axios.get('http://localhost:5000/api/lancamento').then(response => {
            dispatch({type: GET_LANCAMENTOS, payload: response.data})
        }).catch(err => {
            console.log(err)
        })
    }
)


export const ImportLancamentos = (data = []) => (
    dispatch => {
        axios.post(`http://localhost:5000/api/lancamento/import/${localStorage.getItem('conta')}/${localStorage.getItem('agencia')}`,data).then(response => {
            alert('arquivo enviado para importação!')
            console.log(response)
        }).catch(err => {
            console.log(err)
        })
    }
)

function Alerta () {
    toastr.warning('ugigigiugiu')
}