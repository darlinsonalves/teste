import { GET_LANCAMENTOS } from "../actions/conta";

export default (state = {data: []}, action) => {
    switch(action.type) {
        case GET_LANCAMENTOS:
            return {
                ...state,
                data: action.payload
            }
        default:
            return state
    }
}