import { GET_CONTAS } from "../actions/conta";
import { LOGIN } from "../actions/login";

export default (state = {data: [], logado: false}, action) => {
    switch(action.type) {
        case GET_CONTAS:
            return {
                ...state,
                data: action.payload
            }
        case LOGIN:
            return {
                ...state,
                logado: action.payload
            }
        default:
            return state
    }
}