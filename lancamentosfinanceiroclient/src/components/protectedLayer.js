/**
|--------------------------------------------------
| LAYOUT PARA ROTAS PROTEGIDAS POR SENHA
|--------------------------------------------------
*/

/* Dependencies */
import React, { Component, Fragment } from 'react'
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { If, Then, Else } from 'react-if'
import PropTypes from 'prop-types'
import { toastr } from 'react-redux-toastr'

export class ProtectedLayout extends Component {
	componentDidMount() {
		if (localStorage.getItem('conta') === null) {
			this.context.router.history.push('/')
        }
        if (localStorage.getItem('agencia') === null) {
			this.context.router.history.push('/')
		}
	}

	static contextTypes = {
		router: PropTypes.object
	}

	render() {
        const { component: Component, ...rest } = this.props       

		if (!window.navigator.onLine) {
			toastr.error(
				'Sem conexão com a internet',
				'Verifique sua conexão com a internet e tente novamente'
			)
		}

		return (
			<Fragment>
                { <Route {...rest} render={matchProps => <Component {...matchProps} />} />}
			    										
			</Fragment>
		)
	}
}