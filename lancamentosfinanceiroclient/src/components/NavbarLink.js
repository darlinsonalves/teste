/* Dependencies */
import React, { Component } from 'react'
import { NavLink } from 'react-router-dom'
import PropTypes from 'prop-types'

export class NavbarLink extends Component {
    static contextTypes = {
        router: PropTypes.object
    }

    render() {
        const { to, className, children, title } = this.props
        const path = this.context.router.route.match.path

        return (
            (path === to) ?
                <div className={`nav-link active c-normal ${className}`} title="Página atual">
                    {children}
                </div>
                :
                <NavLink to={to} className={`nav-link ${className}`} title={title}>
                    {children}
                </NavLink>
        )
    }
}