import React, {Component } from 'react'
import { Field } from 'redux-form'
import { CustomInput, CustomSelect } from './custom-input';


export class ContaForm extends Component {
  
    render(){
        const { handleSubmit, pristine, submitting, postConta } = this.props            
        return(
            <form className="row mt-4" onSubmit={handleSubmit(postConta)}>
            <Field
                label="Banco:"
                name="banco"
                type="text"
                placeholder="Nome Banco"
                component={CustomInput}
                autoFocus
                required
                columns={6}
            />

            <Field
                label="Agencia:"
                name="agencia"
                type="number"
                placeholder="numero agencia"
                component={CustomInput}
                autoFocus
                required
                columns={6}
            />

            <Field
                label="Conta:"
                name="numero"
                type="number"
                placeholder="Numero da conta"
                component={CustomInput}
                autoFocus
                required
                columns={6}
            />

            <Field
                label="Tipo:"
                name="tipo"
                type="select"
                placeholder="Tipo de conta"
                component={CustomSelect}
                autoFocus
                required
                columns={6}
            >
            <option  value='corrente' >corrente</option>
            <option  value='poupanca' >poupanca</option>
            </Field>

            <Field
                label="Responsável:"
                name="responsavel"
                type="text"
                placeholder="Name do responsavel"
                component={CustomInput}
                autoFocus
                required
                columns={6}
            />
            <fieldset className="form-group text-center col-md-7 mr-auto ml-auto">
					<button type="submit" className="btn btn-success text-uppercase px-4" disabled={pristine || submitting}>
						Salvar
					</button>
				</fieldset>
            </form>
        )
    }
}