import React from 'react'

const Item = props => {
    const { conta } = props;
        return (
            <a key={1} className={`list-group-item list-group-item-action d-flex flex-row justify-content-between`} >
                <div className="d-flex flex-column justify-content-center pr-4 text-left">
                    <h5 className="mb-1 text-capitalize">{conta['responsavel']}</h5>
                    <p className="m-0"><span className="icon-mail"> </span> <span className="text-lowercase">{conta['banco']}</span></p>
                    <p className="m-0"><span className="icon-mail"> </span> <span className="text-lowercase">{conta['agencia']}</span></p>
                    <p className="m-0"><span className="icon-phone"> </span> {conta['numero']}</p>
                    <p className="m-0"><span className="icon-place"> </span> {conta['tipo']}</p>
                </div>                
            </a>
        )    
}

export default Item