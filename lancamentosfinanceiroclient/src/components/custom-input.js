import React from 'react'

export const CustomInput = ({
	input,
	label,
	type,
	placeholder,
	columns,
	helpText,
	required,
	disabled,
	notRounded,
	meta: { touched, error, warning }
}) => {
	const errorClass = touched && error ? 'is-invalid' : ''

	const tabenter = event => {
		var tecla = event.keyCode ? event.keyCode : event.which ? event.which : event.charCode
		if (tecla === 13) {
			event.keyCode = 9
		}
		return event.keyCode
	}

	return (
		<fieldset className={`col-md-${columns} form-group`}>
			{label && (
				<label>
					<strong>{label}</strong>
				</label>
			)}
			<div className={`${required ? 'input-required' : ''}`}>
				<input
					{...input}
					placeholder={placeholder}
					type={type}
					className={`${errorClass} ml-auto mr-auto form-control ${notRounded ? 'rounded-0' : ''}`}
					onKeyUp={tabenter}
					disabled={disabled}
				/>
				{helpText && <small className="form-text text-muted">{helpText}</small>}
				{touched &&
					((error && (
						<div className="text-danger">
							<span className="icon-error" /> {error}
						</div>
					)) ||
						(warning && (
							<div className="text-warning">
								<span className="icon-warning" /> {warning}
							</div>
						)))}
			</div>
		</fieldset>
	)
}


export const CustomSelect = ({
	input,
	label,
	placeholder,
	columns,
	helpText,
	children,
	required,
	disabled,
	notRounded,
	meta: { touched, error, warning }
}) => {
	const errorClass = touched && error ? 'is-invalid' : ''

	return (
		<fieldset className={`col-md-${columns} form-group`}>
			{label && (
				<label>
					<strong>{label}</strong>
				</label>
			)}
			<div className={`${required ? 'input-required' : ''}`}>
				<select
					{...input}
					className={`${errorClass} form-control ${notRounded ? 'rounded-0' : ''}`}
					disabled={disabled}
				>
					<option value="" disabled>
						{placeholder}
					</option>
					{children}
				</select>
				{helpText && <small className="form-text text-muted">{helpText}</small>}
				{touched &&
					((error && (
						<div className="text-danger">
							<span className="icon-error" /> {error}
						</div>
					)) ||
						(warning && (
							<div className="text-warning">
								<span className="icon-warning" /> {warning}
							</div>
						)))}
			</div>
		</fieldset>
	)
}