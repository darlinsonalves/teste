import React from 'react'
import { Link } from 'react-router-dom'

export class App extends React.Component {
    render() {
      return (
        <nav class="navbar navbar-default">
          <div class="container-fluid">
            <div class="navbar-header">
              <a class="navbar-brand" href="#">Financeiro</a>
            </div>
              <ul className="nav navbar-nav" >
                <li><Link to="/">Login</Link></li>
                <li><Link to="/cadastrar">Cadastrar nova conta</Link></li>
                <li><Link to="/list">Lista de contas Cadastrada</Link></li>
                <li><Link to="/import">Importa Lançamentos</Link></li>
                <li><Link to="/lancamentos">Lançamentos</Link></li>
                <li><Link to="/dashboard">DashBoard</Link></li>
                <li><Link to="/" onClick={() => localStorage.clear()} >sair</Link></li>
                
              </ul>
            {this.props.children}
          </div>
        </nav>
      )
    }
  }