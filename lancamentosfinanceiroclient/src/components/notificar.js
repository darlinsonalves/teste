import React, { Component } from 'react'
import Pusher from 'react-pusher'


export class Notificar extends Component {
    constructor(props) {
        super(props)
        this.state = {
            urlRedirect: null
        }
    }

    handleMessage = (data) => {
        console.log(data)
        alert('sistema iniciou um importação!')
    }

    render() {
        if (!localStorage.getItem('jwt'))
            return <div />      

        return(
            <Pusher
                channel={'geral-importacao'}
                event={this.props.evento}
                onUpdate={this.handleMessage}
            />
        )
    }
}