/* Dependencies */
import React from 'react'
import { Field, reduxForm } from 'redux-form'

/* Components */
import { CustomInput } from '../components/custom-input'

export class LoginForm extends React.Component {
    render(){
    const { handleSubmit, pristine, submitting, login, history, logado } = this.props
        if (localStorage.getItem('conta') !== null || logado) {
            history.push('/list')
        }
        return (
            <div className="list-group-item list-group-item-action d-flex flex-row justify-content-between">
            <form className="row mt-4" onSubmit={handleSubmit(login)}>
                <Field
                    label="Agencia:"
                    name="agencia"
                    type="number"
                    placeholder="Sua conta"
                    component={CustomInput}                
                    autoFocus
                    columns={12}
                />

                <Field
                    label="Conta:"
                    name="numero"
                    type="number"
                    placeholder="Sua Agencia"
                    component={CustomInput}
                    columns={12}
                />

                <fieldset className="form-group text-center col-md-7 mr-auto ml-auto">
                    <button
                        type="submit"
                        className="btn btn-lg btn-block btn-rounded btn-success text-white text-uppercase"
                        disabled={pristine || submitting}
                    >
                        {"Entrar"}
                    </button>
                </fieldset>
            </form>
            </div>
        )
    }
}
