import React, {Component} from 'react'
import Item from './componentList';

export class ListaConta extends Component {
    componentWillMount() {
        this.props.getContas()
    }
    render() {
        const {contas, history} = this.props
        if (localStorage.getItem('conta') === null) {
            history.push('/')
        }
        return (            
            contas.map(conta => {
                return <Item conta={conta} />
            })            
        )
    }
}