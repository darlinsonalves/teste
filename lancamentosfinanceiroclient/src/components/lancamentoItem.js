/* Dependencies */
import React, { Component } from 'react'

export class LancamentoItem extends Component {
	
	render() {
        const {lancamento} = this.props;
		return (
			<tr>
				<th className="text-center">
					{new Date(lancamento['data']).toLocaleString('pt-BR')}
				</th>
				<td >{lancamento.tipo}</td>
				<td >{lancamento.valor}</td>
                <td >{lancamento.descricao}</td>
				<td >{lancamento.conta}</td>
				<td >{lancamento.agencia}</td>
			</tr>
		)
	}
}
