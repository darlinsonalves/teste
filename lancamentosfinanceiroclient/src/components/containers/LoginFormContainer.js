import { connect } from "react-redux";
import { reduxForm } from "redux-form";
import { LoginForm } from "../login-form";
import {login} from '../../actions/login'
import { bindActionCreators } from "redux";

const form = reduxForm({
    form: 'signin-form'
})(LoginForm)

const mapDispatch = disptch => (
    bindActionCreators({
        login
    }, disptch)
)

const mapState = (state) =>(
    {
        logado: state.contas.logado
    }
)

const LoginFormContainer = connect(mapState, mapDispatch)(form)

export default LoginFormContainer