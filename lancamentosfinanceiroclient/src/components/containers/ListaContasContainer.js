import {connect} from 'react-redux'
import { ListaConta } from '../listacontas';
import { bindActionCreators } from 'redux';
import { getContas } from '../../actions/conta';

const mapState = state => (
    { contas: state.contas.data }
)

const mapDispatch = dispatch => (
    bindActionCreators({
        getContas
    }, dispatch)
)
const ListaContaContainer = connect(mapState, mapDispatch)(ListaConta)

export default ListaContaContainer