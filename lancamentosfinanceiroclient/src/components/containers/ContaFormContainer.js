import { reduxForm } from "redux-form";
import { ContaForm } from "../ContaForm";
import { connect } from "react-redux";

import {postConta} from '../../actions/conta'
import { bindActionCreators } from "redux";

const mapDispatch = dispatch =>(
     bindActionCreators({
        postConta
     }, dispatch)
)

const form = reduxForm({
    form: 'conta-form'
})(ContaForm)

const ContaFormContainer = connect(null, mapDispatch)(form)

export default ContaFormContainer