import React, { Component } from 'react'
import CsvParse from '@vtex/react-csv-parse'
 
export class ImportCSV extends Component {
    state ={
        data: []
    }
    handleData = data => {
        this.setState({ data })        
    }

    sendData = () => {
        this.props.ImportLancamentos(this.state.data)
    }
 
    render() {
        const {history} = this.props;
        const keys = [
            "data",
            "tipo",
            "valor",
            "descricao"
          ]

        if (localStorage.getItem('conta') === null) {
            history.push('/')
        }
        return (
            <div className="list-group-item list-group-item-action d-flex flex-row justify-content-between">    
                <CsvParse
                    keys={keys}
                    onDataUploaded={this.handleData}
                    onError={this.handleError}
                    render={onChange => <input type="file" onChange={onChange} />}
                    />
                 <fieldset className="form-group text-center col-md-7 mr-auto ml-auto">
					<button onClick={this.sendData } type="button" className="btn btn-success text-uppercase px-4" disabled={!this.state.data.length}>
						Importa lancamentos
					</button>
				</fieldset>
            </div>
        )
    }
}
