import React, { Component, Fragment } from 'react'
import { Notificar } from './notificar';



export default class Notificacoes extends Component {
	componentDidMount = () => {
		try {
			Notification.requestPermission()
		} catch (e) {
			
		}
	}

	render() {
		if (!localStorage.getItem('conta')) return <div />

		return (
			<Fragment>				
                <Notificar evento="importacao" />
			</Fragment>
		)
	}
}
