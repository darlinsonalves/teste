export const INITIAL_STATE = {
    usuario: null,
    isLoading: false,
    loading: false,
}

export const LOADING_USER = 'LOADING_USER'
export const GET_USER_SUCCESS = 'GET_USER_SUCCESS'
export const GET_USER_ERROR = 'GET_USER_ERROR'
export const LOGOUT = 'LOGOUT';
export const START_UPDATING_DADOS_USUARIO = 'START_UPDATING_DADOS_USUARIO'